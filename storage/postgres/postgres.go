package postgres

import (
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"car_24/car_24_user_service.git/config"
	"car_24/car_24_user_service.git/storage"
)

type Store struct {
	db       *pgxpool.Pool
	user     storage.UserRepoI
	region   storage.RegionRepoI
	district storage.DistrictRepoI
	client   storage.ClientRepoI
	invesor  storage.InvestorRepoI
	mechanic storage.MechanicRepoI
}

type Pool struct {
	db *pgxpool.Pool
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	))
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections

	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (l *Store) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	args := make([]interface{}, 0, len(data)+2) // making space for arguments + level + msg
	args = append(args, level, msg)
	for k, v := range data {
		args = append(args, fmt.Sprintf("%s=%v", k, v))
	}
	log.Println(args...)
}

func (s *Store) User() storage.UserRepoI {
	if s.user == nil {
		s.user = NewUserRepo(s.db)
	}

	return s.user
}

func (s *Store) Region() storage.RegionRepoI {
	if s.region == nil {
		s.region = NewRegionRepo(s.db)
	}

	return s.region
}

func (s *Store) District() storage.DistrictRepoI {
	if s.district == nil {
		s.district = NewDistrictRepo(s.db)
	}

	return s.district
}

func (s *Store) Client() storage.ClientRepoI {
	if s.client == nil {
		s.client = NewClientRepo(s.db)
	}

	return s.client
}

func (s *Store) Investor() storage.InvestorRepoI {
	if s.invesor == nil {
		s.invesor = NewInvestorRepo(s.db)
	}

	return s.invesor
}

func (s *Store) Mechanic() storage.MechanicRepoI {
	if s.mechanic == nil {
		s.mechanic = NewMechanicRepo(s.db)
	}

	return s.mechanic
}