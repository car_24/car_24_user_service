package postgres

import (
	"car_24/car_24_user_service.git/genproto/user_service"
	"car_24/car_24_user_service.git/models"
	"car_24/car_24_user_service.git/pkg/helper"
	"car_24/car_24_user_service.git/storage"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type MechanicRepo struct {
	db *pgxpool.Pool
}

func NewMechanicRepo(db *pgxpool.Pool) storage.MechanicRepoI {
	return &MechanicRepo{
		db: db,
	}
}

func (c *MechanicRepo) Create(ctx context.Context, req *user_service.CreateMechanic) (resp *user_service.MechanicPrimaryKey, err error) {

	var id = uuid.New().String()

	query := `INSERT INTO "mechanics" (
		mechanic_id,
		first_name,
		last_name,
		phone_number,
		photo,
		updated_at
	) VALUES ($1, $2, $3, $4, $5, now())
	`

	_, err = c.db.Exec(ctx, query,
		id,
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
		req.Photo,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.MechanicPrimaryKey{MechanicId: id}, nil
}

func (c *MechanicRepo) GetByPKey(ctx context.Context, req *user_service.MechanicPrimaryKey) (*user_service.Mechanic, error) {

	query := `
		SELECT
			mechanic_id,
			COALESCE(first_name, ''),
			COALESCE(last_name, ''),
			COALESCE(photo, ''),
			phone_number,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "mechanics"
		WHERE mechanic_id = $1
	`
	var resp user_service.Mechanic

	err := c.db.QueryRow(ctx, query, req.GetMechanicId()).Scan(
		&resp.MechanicId,
		&resp.FirstName,
		&resp.LastName,
		&resp.Photo,
		&resp.PhoneNumber,
		&resp.CreatedAt,
		&resp.UpdatedAt,
		&resp.DeletedAt,
	)

	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *MechanicRepo) GetAll(ctx context.Context, req *user_service.GetListMechanicRequest) (resp *user_service.GetListMechanicResponse, err error) {

	resp = &user_service.GetListMechanicResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " AND TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			mechanic_id,
			COALESCE(first_name, ''),
			COALESCE(last_name, ''),
			COALESCE(photo, ''),
			phone_number,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "mechanics"
		WHERE deleted_at is null
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetSearch()) > 0 {
		filter = " WHERE first_name||' '||last_name ILIKE" + "'%" + req.Search + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var user user_service.Mechanic

		err := rows.Scan(
			&resp.Count,
			&user.MechanicId,
			&user.FirstName,
			&user.LastName,
			&user.Photo,
			&user.PhoneNumber,
			&user.CreatedAt,
			&user.UpdatedAt,
			&user.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Mechanics = append(resp.Mechanics, &user)
	}

	return
}

func (c *MechanicRepo) Update(ctx context.Context, req *user_service.UpdateMechanic) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "mechanics"
			SET
				first_name = $1,
				last_name = $2,
				photo = $3,
				phone_number = $4,
				updated_at = now()
			WHERE
				mechanic_id = $5`

	result, err := c.db.Exec(ctx, query,
		req.FirstName,
		req.LastName,
		req.Photo,
		req.PhoneNumber,
		req.MechanicId,
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *MechanicRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["mechanic_id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"mechanics"
	` + set + ` , updated_at = now()
		WHERE
			mechanic_id = :mechanic_id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *MechanicRepo) Delete(ctx context.Context, req *user_service.MechanicPrimaryKey) error {

	query := `UPDATE mechanics SET deleted_at = now() where mechanic_id = $1`

	_, err := c.db.Exec(ctx, query, req.MechanicId)

	if err != nil {
		return err
	}

	return nil
}
