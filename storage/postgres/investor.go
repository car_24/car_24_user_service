package postgres

import (
	"car_24/car_24_user_service.git/genproto/user_service"
	"car_24/car_24_user_service.git/models"
	"car_24/car_24_user_service.git/pkg/helper"
	"car_24/car_24_user_service.git/storage"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type InvestorRepo struct {
	db *pgxpool.Pool
}

func NewInvestorRepo(db *pgxpool.Pool) storage.InvestorRepoI {
	return &InvestorRepo{
		db: db,
	}
}

func (c *InvestorRepo) Create(ctx context.Context, req *user_service.CreateInvestor) (resp *user_service.InvestorPrimaryKey, err error) {

	var id = uuid.New().String()

	query := `INSERT INTO "investors" (
		investor_id,
		first_name,
		last_name,
		address,
		description,
		photo,
		login,
		type,
		phone_number,
		date_of_birth,
		updated_at
	) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, now())
	`	

	fmt.Println(query)

	_, err = c.db.Exec(ctx, query,
		id,
		req.FirstName,
		req.LastName,
		req.Address,
		req.Description,
		req.Photo,
		req.Login,
		req.Type,
		req.PhoneNumber,
		req.DateOfBirth,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.InvestorPrimaryKey{InvestorId: id}, nil
}

func (c *InvestorRepo) GetByPKey(ctx context.Context, req *user_service.InvestorPrimaryKey) (*user_service.Investor, error) {

	query := `
		SELECT
			investor_id,
			COALESCE(first_name, ''),
			COALESCE(last_name, ''),
			address,
			description,
			COALESCE(photo, ''),
			COALESCE(status, 0),
			login,
			type,
			phone_number,
			CAST(date_of_birth AS VARCHAR),
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "investors"
		WHERE investor_id = $1
	`
	var resp user_service.Investor

	err := c.db.QueryRow(ctx, query, req.GetInvestorId()).Scan(
		&resp.InvestorId,
		&resp.FirstName,
		&resp.LastName,
		&resp.Address,
		&resp.Description,
		&resp.Photo,
		&resp.Status,
		&resp.Login,
		&resp.Type,
		&resp.PhoneNumber,
		&resp.DateOfBirth,
		&resp.CreatedAt,
		&resp.UpdatedAt,
		&resp.DeletedAt,
	)

	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *InvestorRepo) GetAll(ctx context.Context, req *user_service.GetListInvestorRequest) (resp *user_service.GetListInvestorResponse, err error) {

	resp = &user_service.GetListInvestorResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			investor_id,
			COALESCE(first_name, ''),
			COALESCE(last_name, ''),
			address,
			description,
			COALESCE(photo, ''),
			COALESCE(status, 0),
			login,
			type,
			phone_number,
			CAST(date_of_birth AS VARCHAR),
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "investors"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetSearch()) > 0 {
		filter = " WHERE first_name||' '||last_name ILIKE" + "'%" + req.Search + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var user user_service.Investor

		err := rows.Scan(
			&resp.Count,
			&user.InvestorId,
			&user.FirstName,
			&user.LastName,
			&user.Address,
			&user.Description,
			&user.Photo,
			&user.Status,
			&user.Login,
			&user.Type,
			&user.PhoneNumber,
			&user.DateOfBirth,
			&user.CreatedAt,
			&user.UpdatedAt,
			&user.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Investors = append(resp.Investors, &user)
	}

	return
}

func (c *InvestorRepo) Update(ctx context.Context, req *user_service.UpdateInvestor) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "investors"
			SET
				first_name = :first_name,
				last_name = :last_name,
				address = :address,
				description = :description,
				photo = :photo,
				login = :login,
				type = :type,
				phone_number = :phone_number,
				date_of_birth = :date_of_birth,
				updated_at = now()
			WHERE
				investor_id = :investor_id`
	params = map[string]interface{}{
		"investor_id":  req.GetInvestorId(),
		"first_name":   req.GetFirstName(),
		"last_name":    req.GetLastName(),
		"address":    req.GetAddress(),
		"description":    req.GetDescription(),
		"photo":        req.GetPhoto(),
		"login":        req.GetLogin(),
		"type":        req.GetType(),
		"phone_number": req.GetPhoneNumber(),
		"date_of_birth" : req.GetDateOfBirth(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *InvestorRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["investor_id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"investors"
	` + set + ` , updated_at = now()
		WHERE
			investor_id = :investor_id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *InvestorRepo) Delete(ctx context.Context, req *user_service.InvestorPrimaryKey) error {

	query := `DELETE FROM "investors" WHERE investor_id = $1`

	_, err := c.db.Exec(ctx, query, req.InvestorId)

	if err != nil {
		return err
	}

	return nil
}
