package postgres

import (
	"car_24/car_24_user_service.git/genproto/user_service"
	"car_24/car_24_user_service.git/models"
	"car_24/car_24_user_service.git/pkg/helper"
	"car_24/car_24_user_service.git/storage"
	"context"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type ClientRepo struct {
	db *pgxpool.Pool
}

func NewClientRepo(db *pgxpool.Pool) storage.ClientRepoI {
	return &ClientRepo{
		db: db,
	}
}

func (c *ClientRepo) Create(ctx context.Context, req *user_service.CreateClient) (resp *user_service.ClientPrimaryKey, err error) {

	var id = uuid.New().String()

	query := `INSERT INTO "clients" (
		id,
		first_name,
		last_name,
		phone_number,
		photo,
		updated_at
	) VALUES ($1, $2, $3, $4, $5, now())
	`

	_, err = c.db.Exec(ctx, query,
		id,
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
		req.Photo,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.ClientPrimaryKey{ClientId: id}, nil
}

func (c *ClientRepo) GetByPKey(ctx context.Context, req *user_service.ClientPrimaryKey) (*user_service.Client, error) {

	query := `
		SELECT
			id,
			COALESCE(first_name, ''),
			COALESCE(last_name, ''),
			phone_number,
			COALESCE(status, 0),
			COALESCE(photo, ''),
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "clients"
		WHERE id = $1
	`
	var resp user_service.Client

	err := c.db.QueryRow(ctx, query, req.GetClientId()).Scan(
		&resp.ClientId,
		&resp.FirstName,
		&resp.LastName,
		&resp.PhoneNumber,
		&resp.Status,
		&resp.Photo,
		&resp.CreatedAt,
		&resp.UpdatedAt,
		&resp.DeletedAt,
	)

	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *ClientRepo) GetAll(ctx context.Context, req *user_service.GetListClientRequest) (resp *user_service.GetListClientResponse, err error) {

	resp = &user_service.GetListClientResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			first_name,
			last_name,
			phone_number,
			COALESCE(status, 0),
			COALESCE(photo, ''),
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
			COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
		FROM "clients"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetSearch()) > 0 {
		filter = " WHERE first_name||' '||last_name ILIKE" + "'%" + req.Search + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var user user_service.Client

		err := rows.Scan(
			&resp.Count,
			&user.ClientId,
			&user.FirstName,
			&user.LastName,
			&user.PhoneNumber,
			&user.Status,
			&user.Photo,
			&user.CreatedAt,
			&user.UpdatedAt,
			&user.DeletedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Clients = append(resp.Clients, &user)
	}

	return
}

func (c *ClientRepo) Update(ctx context.Context, req *user_service.UpdateClient) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "clients"
			SET
				first_name = :first_name,
				last_name = :last_name,
				phone_number = :phone_number,
				photo = :photo,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetClientId(),
		"first_name":   req.GetFirstName(),
		"last_name":    req.GetLastName(),
		"phone_number": req.GetPhoneNumber(),
		"photo":        req.GetPhoto(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ClientRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"clients"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *ClientRepo) Delete(ctx context.Context, req *user_service.ClientPrimaryKey) error {

	query := `DELETE FROM "clients" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.ClientId)

	if err != nil {
		return err
	}

	return nil
}
