package storage

import (
	"car_24/car_24_user_service.git/genproto/user_service"
	"car_24/car_24_user_service.git/models"
	"context"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
	Region() RegionRepoI
	District() DistrictRepoI
	Client() ClientRepoI
	Investor() InvestorRepoI
	Mechanic() MechanicRepoI
}

type RegionRepoI interface {
	Create(ctx context.Context, req *user_service.CreateRegion) (resp *user_service.RegionPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.RegionPrimaryKey) (resp *user_service.Region, err error)
	GetAll(ctx context.Context, req *user_service.GetListRegionRequest) (resp *user_service.GetListRegionResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateRegion) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.RegionPrimaryKey) error
}

type DistrictRepoI interface {
	Create(ctx context.Context, req *user_service.CreateDistrict) (resp *user_service.DistrictPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.DistrictPrimaryKey) (resp *user_service.District, err error)
	GetAll(ctx context.Context, req *user_service.GetListDistrictRequest) (resp *user_service.GetListDistrictResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateDistrict) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.DistrictPrimaryKey) error
}

type UserRepoI interface {
	Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.UserPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error)
	GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateUser) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.UserPrimaryKey) error
	Login(ctx context.Context, req *user_service.LoginReq) (*user_service.User, error)
}

type ClientRepoI interface {
	Create(ctx context.Context, req *user_service.CreateClient) (resp *user_service.ClientPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.ClientPrimaryKey) (resp *user_service.Client, err error)
	GetAll(ctx context.Context, req *user_service.GetListClientRequest) (resp *user_service.GetListClientResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateClient) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.ClientPrimaryKey) error
}

type InvestorRepoI interface {
	Create(ctx context.Context, req *user_service.CreateInvestor) (resp *user_service.InvestorPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.InvestorPrimaryKey) (resp *user_service.Investor, err error)
	GetAll(ctx context.Context, req *user_service.GetListInvestorRequest) (resp *user_service.GetListInvestorResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateInvestor) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.InvestorPrimaryKey) error
}

type MechanicRepoI interface {
	Create(ctx context.Context, req *user_service.CreateMechanic) (resp *user_service.MechanicPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.MechanicPrimaryKey) (resp *user_service.Mechanic, err error)
	GetAll(ctx context.Context, req *user_service.GetListMechanicRequest) (resp *user_service.GetListMechanicResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateMechanic) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.MechanicPrimaryKey) error
}
