package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"


	"car_24/car_24_user_service.git/config"
	"car_24/car_24_user_service.git/genproto/user_service"
	"car_24/car_24_user_service.git/grpc/client"
	"car_24/car_24_user_service.git/grpc/service"
	"car_24/car_24_user_service.git/pkg/logger"
	"car_24/car_24_user_service.git/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	user_service.RegisterUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))
	user_service.RegisterRegionServiceServer(grpcServer, service.NewRegionService(cfg, log, strg, srvc))
	user_service.RegisterDistrictServiceServer(grpcServer, service.NewDistrictService(cfg, log, strg, srvc))
	user_service.RegisterClientServiceServer(grpcServer, service.NewClientService(cfg, log, strg, srvc))
	user_service.RegisterInvestorServiceServer(grpcServer, service.NewInvestorService(cfg, log, strg, srvc))
	user_service.RegisterMechanicServiceServer(grpcServer, service.NewMechanicService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
