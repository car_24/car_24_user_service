package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"car_24/car_24_user_service.git/config"
	"car_24/car_24_user_service.git/genproto/user_service"
	"car_24/car_24_user_service.git/grpc/client"
	"car_24/car_24_user_service.git/pkg/logger"
	"car_24/car_24_user_service.git/storage"
)

type RegionService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedRegionServiceServer
}

func NewRegionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *RegionService {
	return &RegionService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *RegionService) Create(ctx context.Context, req *user_service.CreateRegion) (resp *user_service.Region, err error) {

	i.log.Info("---CreateRegion------>", logger.Any("req", req))

	pKey, err := i.strg.Region().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateRegion->Region->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Region().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyRegion->Region->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *RegionService) GetById(ctx context.Context, req *user_service.RegionPrimaryKey) (resp *user_service.Region, err error) {

	i.log.Info("---GetRegionByID------>", logger.Any("req", req))

	resp, err = i.strg.Region().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetUserByID->Region->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *RegionService) GetList(ctx context.Context, req *user_service.GetListRegionRequest) (resp *user_service.GetListRegionResponse, err error) {

	i.log.Info("---GetRegions------>", logger.Any("req", req))

	resp, err = i.strg.Region().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetRegion->Region->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *RegionService) Update(ctx context.Context, req *user_service.UpdateRegion) (resp *user_service.Region, err error) {

	i.log.Info("---UpdateRegion------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Region().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateRegion--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Region().GetByPKey(ctx, &user_service.RegionPrimaryKey{RegionId: req.RegionId})
	if err != nil {
		i.log.Error("!!!GetRegion->Region->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *RegionService) Delete(ctx context.Context, req *user_service.RegionPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteRegion------>", logger.Any("req", req))

	err = i.strg.Region().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteRegion->Region->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
