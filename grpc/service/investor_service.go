
package service

import (
	"car_24/car_24_user_service.git/config"
	"car_24/car_24_user_service.git/genproto/user_service"
	"car_24/car_24_user_service.git/grpc/client"
	"car_24/car_24_user_service.git/models"
	"car_24/car_24_user_service.git/pkg/logger"
	"car_24/car_24_user_service.git/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type InvestorService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedInvestorServiceServer
}

func NewInvestorService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *InvestorService {
	return &InvestorService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *InvestorService) Create(ctx context.Context, req *user_service.CreateInvestor) (resp *user_service.Investor, err error) {

	i.log.Info("---CreateInvestor------>", logger.Any("req", req))

	pKey, err := i.strg.Investor().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateInvestor->Investor->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Investor().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyInvestor->Investor->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *InvestorService) GetById(ctx context.Context, req *user_service.InvestorPrimaryKey) (resp *user_service.Investor, err error) {

	i.log.Info("---GetInvestorByID------>", logger.Any("req", req))

	resp, err = i.strg.Investor().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetInvestorByID->Investor->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *InvestorService) GetList(ctx context.Context, req *user_service.GetListInvestorRequest) (resp *user_service.GetListInvestorResponse, err error) {

	i.log.Info("---GetInvestors------>", logger.Any("req", req))

	resp, err = i.strg.Investor().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetInvestors->Investor->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *InvestorService) Update(ctx context.Context, req *user_service.UpdateInvestor) (resp *user_service.Investor, err error) {

	i.log.Info("---UpdateInvestor------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Investor().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateInvestor--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Investor().GetByPKey(ctx, &user_service.InvestorPrimaryKey{InvestorId: req.InvestorId})
	if err != nil {
		i.log.Error("!!!GetInvestor->Investor->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *InvestorService) UpdatePatch(ctx context.Context, req *user_service.UpdatePatchInvestor) (resp *user_service.Investor, err error) {

	i.log.Info("---UpdatePatchEnrolledStudent------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Investor().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchInvestor--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Investor().GetByPKey(ctx, &user_service.InvestorPrimaryKey{InvestorId: req.Id})
	if err != nil {
		i.log.Error("!!!GetInvestor->Investor->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *InvestorService) Delete(ctx context.Context, req *user_service.InvestorPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteInvestor------>", logger.Any("req", req))

	err = i.strg.Investor().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteInvestor->Investor->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
