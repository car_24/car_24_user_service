package service

import (
	"car_24/car_24_user_service.git/config"
	"car_24/car_24_user_service.git/genproto/user_service"
	"car_24/car_24_user_service.git/grpc/client"
	"car_24/car_24_user_service.git/models"
	"car_24/car_24_user_service.git/pkg/logger"
	"car_24/car_24_user_service.git/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ClientService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedClientServiceServer
}

func NewClientService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ClientService {
	return &ClientService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ClientService) Create(ctx context.Context, req *user_service.CreateClient) (resp *user_service.Client, err error) {

	i.log.Info("---CreateClient------>", logger.Any("req", req))

	pKey, err := i.strg.Client().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateClient->Client->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Client().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyClient->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ClientService) GetById(ctx context.Context, req *user_service.ClientPrimaryKey) (resp *user_service.Client, err error) {

	i.log.Info("---GetClientByID------>", logger.Any("req", req))

	resp, err = i.strg.Client().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetClientByID->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ClientService) GetList(ctx context.Context, req *user_service.GetListClientRequest) (resp *user_service.GetListClientResponse, err error) {

	i.log.Info("---GetClients------>", logger.Any("req", req))

	resp, err = i.strg.Client().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetClients->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ClientService) Update(ctx context.Context, req *user_service.UpdateClient) (resp *user_service.Client, err error) {

	i.log.Info("---UpdateClient------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Client().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateClient--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Client().GetByPKey(ctx, &user_service.ClientPrimaryKey{ClientId: req.ClientId})
	if err != nil {
		i.log.Error("!!!GetClient->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ClientService) UpdatePatch(ctx context.Context, req *user_service.UpdatePatchClient) (resp *user_service.Client, err error) {

	i.log.Info("---UpdatePatchEnrolledStudent------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Client().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchClient--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Client().GetByPKey(ctx, &user_service.ClientPrimaryKey{ClientId: req.Id})
	if err != nil {
		i.log.Error("!!!GetClient->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ClientService) Delete(ctx context.Context, req *user_service.ClientPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteClient------>", logger.Any("req", req))

	err = i.strg.Client().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteClient->Client->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
