package service

import (
	"car_24/car_24_user_service.git/config"
	"car_24/car_24_user_service.git/genproto/user_service"
	"car_24/car_24_user_service.git/grpc/client"
	"car_24/car_24_user_service.git/models"
	"car_24/car_24_user_service.git/pkg/logger"
	"car_24/car_24_user_service.git/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type MechanicService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedMechanicServiceServer
}

func NewMechanicService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *MechanicService {
	return &MechanicService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *MechanicService) Create(ctx context.Context, req *user_service.CreateMechanic) (resp *user_service.Mechanic, err error) {

	i.log.Info("---CreateMechanic------>", logger.Any("req", req))

	pKey, err := i.strg.Mechanic().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateMechanic->Mechanic->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Mechanic().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyMechanic->Mechanic->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *MechanicService) GetById(ctx context.Context, req *user_service.MechanicPrimaryKey) (resp *user_service.Mechanic, err error) {

	i.log.Info("---GetMechanicByID------>", logger.Any("req", req))

	resp, err = i.strg.Mechanic().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetMechanicByID->Mechanic->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *MechanicService) GetList(ctx context.Context, req *user_service.GetListMechanicRequest) (resp *user_service.GetListMechanicResponse, err error) {

	i.log.Info("---GetMechanics------>", logger.Any("req", req))

	resp, err = i.strg.Mechanic().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetMechanics->Mechanic->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *MechanicService) Update(ctx context.Context, req *user_service.UpdateMechanic) (resp *user_service.Mechanic, err error) {

	i.log.Info("---UpdateMechanic------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Mechanic().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateMechanic--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Mechanic().GetByPKey(ctx, &user_service.MechanicPrimaryKey{MechanicId: req.MechanicId})
	if err != nil {
		i.log.Error("!!!GetMechanic->Mechanic->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *MechanicService) UpdatePatch(ctx context.Context, req *user_service.UpdatePatchMechanic) (resp *user_service.Mechanic, err error) {

	i.log.Info("---UpdatePatchEnrolledStudent------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Mechanic().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchMechanic--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Mechanic().GetByPKey(ctx, &user_service.MechanicPrimaryKey{MechanicId: req.Id})
	if err != nil {
		i.log.Error("!!!GetMechanic->Mechanic->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *MechanicService) Delete(ctx context.Context, req *user_service.MechanicPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteMechanic------>", logger.Any("req", req))

	err = i.strg.Mechanic().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteMechanic->Mechanic->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
